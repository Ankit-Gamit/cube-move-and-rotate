using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private bool doMove = true;
    [SerializeField] private Vector3 _direction = Vector3.back;
    [SerializeField] private float _speed = 10f;

    [Header("Rotation")]
    [SerializeField] private bool doRotate = true;
    [SerializeField] private float _rotationSpeed = 500f;

    private void Update()
    {
        if (doMove) MovePrefab();
        if (doRotate) RotatePrefab();
    }

    private void MovePrefab()
    {
        transform.Translate(_direction * _speed * Time.deltaTime);
    }

    private void RotatePrefab()
    {
        transform.GetChild(0).Rotate(Vector3.up, _rotationSpeed * Time.deltaTime);
    }
}
